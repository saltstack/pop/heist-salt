from unittest.mock import Mock
from unittest.mock import patch

import pytest


@pytest.mark.parametrize(
    "version,exp_ret",
    [
        ("3002.3", True),
        ("3003", False),
        ("3003.1", False),
        ("3002.8", True),
        ("3003rc1", False),
    ],
)
def test_old_salt(hub, mock_hub, tmp_path, version, exp_ret):
    """
    test version comparison
    """
    mock_hub.salt.key.local_master.old_salt = hub.salt.key.local_master.old_salt
    with patch("salt.version.__version__", version):
        if not exp_ret:
            assert not mock_hub.salt.key.local_master.old_salt()
        else:
            assert mock_hub.salt.key.local_master.old_salt()


def test_delete_minion_old_salt(hub, mock_hub, tmp_path):
    """
    test delete_minion on an old version of salt
    """
    mock_hub.salt.key.local_master.delete_minion = (
        hub.salt.key.local_master.delete_minion
    )
    mock_hub.salt.key.local_master.old_salt = hub.salt.key.local_master.old_salt
    mock_hub.salt.key.local_master.DEFAULT_MASTER_CONFIG = tmp_path / "master"
    with patch("salt.key.get_key", Mock()):
        with patch("salt.version.__version__", "3002.3"):
            assert mock_hub.salt.key.local_master.delete_minion("test_minion")
