from unittest.mock import call

import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize("status", [True, False])
@pytest.mark.parametrize("target_os", ["linux", "windows"])
async def test_start(hub, mock_hub, tmp_path, target_os, status):
    """
    test starting the minion
    """
    target_name = "test_target"
    tunnel_plugin = "asyncssh"
    service_plugin = "raw"
    run_dir = tmp_path / "run_dir"
    mock_hub.service.salt.init.start = hub.service.salt.init.start
    mock_hub.heist.salt.minion.raw_run_cmd = hub.heist.salt.minion.raw_run_cmd
    mock_hub.service.raw.status.return_value = status
    mock_hub.tool.artifacts.target_conf = hub.tool.artifacts.target_conf
    mock_hub.heist.salt.minion.get_service_name.return_value = "salt-minion"
    mock_hub.tool.artifacts.get_salt_path.return_value = run_dir / "salt"
    await mock_hub.service.salt.init.start(
        target_name=target_name,
        tunnel_plugin=tunnel_plugin,
        service_plugin=service_plugin,
        run_dir=run_dir,
        target_os=target_os,
    )
    if not status:
        run_cmd = (
            f"{run_dir / 'salt'/ 'salt-minion'} -c {str(run_dir / 'root_dir' / 'conf')}"
        )
        if target_os == "linux":
            run_cmd = run_cmd + " -d"
        assert mock_hub.service.raw.start.call_args == call(
            target_name,
            tunnel_plugin,
            "salt-minion",
            run_cmd=run_cmd,
            block=False,
            target_os=target_os,
            run_dir=run_dir,
        )
    else:
        mock_hub.service.raw.start.assert_not_called()


def test_raw_run_cmd(hub, mock_hub, tmp_path):
    service_plugin = "raw"
    run_dir = tmp_path / "run_dir"
    target_os = "linux"
    mock_hub.heist.salt.minion.raw_run_cmd = hub.heist.salt.minion.raw_run_cmd
    mock_hub.tool.artifacts.get_salt_path = hub.tool.artifacts.get_salt_path
    mock_hub.heist.salt.minion.get_service_name.return_value = "salt-minion"
    mock_hub.tool.artifacts.target_conf = hub.tool.artifacts.target_conf
    ret = mock_hub.heist.salt.minion.raw_run_cmd(
        service_plugin, run_dir, target_os=target_os
    )
    run_cmd = f"{str(run_dir / 'salt' / 'salt-minion')} -c {str(run_dir / 'root_dir' / 'conf')}"
    if target_os == "linux":
        run_cmd = run_cmd + " -d"
    assert ret == run_cmd
