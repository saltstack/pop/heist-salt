from unittest.mock import call

import pytest


@pytest.mark.parametrize("target_os", ["linux", "windows"])
@pytest.mark.parametrize(
    "service_plugin,exp_ret", [("systemd", "salt-proxy"), ("raw", "salt-proxy")]
)
def test_get_service_name(hub, mock_hub, service_plugin, exp_ret, target_os):
    """
    Test get_service_name
    """
    mock_hub.heist.salt.proxy.get_service_name = hub.heist.salt.proxy.get_service_name
    assert (
        mock_hub.heist.salt.proxy.get_service_name(service_plugin, target_os=target_os)
        == exp_ret
    )


@pytest.mark.asyncio
async def test_proxy_run(hub, mock_hub, test_data):
    """
    test the run function for the salt.proxy
    manager
    """
    mock_hub.heist.salt.proxy.run = hub.heist.salt.proxy.run
    await mock_hub.heist.salt.proxy.run(remotes=test_data.roster)
    assert mock_hub.heist.salt.init.run.call_args == call(
        test_data.roster, artifact_version=None, manage_service=None
    )


@pytest.mark.asyncio
async def test_proxy_clean(hub, mock_hub, test_data):
    """
    test the clean function for the salt.proxy
    manager
    """
    proxy = "test_proxy"
    mock_hub.heist.CONS = test_data.CONS
    mock_hub.heist.CONS[test_data.tname]["target_id"] = proxy
    mock_hub.heist.salt.proxy.clean = hub.heist.salt.proxy.clean
    await mock_hub.heist.salt.proxy.clean(
        test_data.tname, test_data.tunnel_plugin, service_plugin="raw", vals={}
    )
    assert mock_hub.heist.salt.init.clean.call_args == call(
        test_data.tname, test_data.tunnel_plugin, "raw", {}
    )


@pytest.mark.parametrize("bootstrap", [True, False])
def test_get_salt_opts_proxy(hub, mock_hub, test_data, tmp_path, bootstrap):
    """
    test get_salt_opts function for salt.proxy manager
    """
    run_dir = tmp_path / "run_dir"
    proxy = "test_proxy"
    mock_hub.heist.salt.proxy.get_salt_opts = hub.heist.salt.proxy.get_salt_opts
    conf_file, opts = mock_hub.heist.salt.proxy.get_salt_opts(
        run_dir,
        test_data.tname,
        target_os="linux",
        target_id=proxy,
        bootstrap=bootstrap,
    )

    ret_dict = {
        "root_dir": str(run_dir / "root_dir"),
        "id": proxy,
        "grains": {"minion_type": "heist_proxy"},
    }
    if not bootstrap:
        bootstrap_dict = {
            "master": "127.0.0.1",
            "master_port": 44506,
            "publish_port": 44505,
        }
        ret_dict = {**ret_dict, **bootstrap_dict}
        assert opts == ret_dict
    else:
        assert opts == ret_dict
    assert conf_file == "proxy"


@pytest.mark.asyncio
async def test_proxy_tunnel_to_ports(hub, mock_hub, test_data):
    """
    test the tunnel_to_ports function for the salt.proxy
    manager
    """
    salt_conf = {"publish_port": 1234, "master_port": 5678, "id": "proxy"}
    mock_hub.heist.salt.proxy.tunnel_to_ports = hub.heist.salt.proxy.tunnel_to_ports
    assert await mock_hub.heist.salt.proxy.tunnel_to_ports(
        test_data.tname, "asyncssh", salt_conf
    )


@pytest.mark.parametrize("target_os", ["linux", "windows"])
def test_proxy_generate_aliases(hub, mock_hub, test_data, tmp_path, target_os):
    """
    test generate_aliases function for salt.proxy manager
    """
    run_dir = tmp_path / "run_dir"
    artifacts = tmp_path / "artifacts"
    manager = "proxy"
    mock_hub.heist.salt.proxy.generate_aliases = hub.heist.salt.proxy.generate_aliases
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts
    aliases, artifacts_dir, content = mock_hub.heist.salt.proxy.generate_aliases(
        run_dir, target_os=target_os
    )
    assert artifacts_dir == artifacts / "scripts" / manager
    if target_os == "linux":
        assert "/bin/bash" in content
    else:
        assert "ECHO OFF" in content
    assert (
        aliases["salt-proxy"]["file"] == artifacts / "scripts" / manager / "salt-proxy"
    )
    assert (
        aliases["salt-proxy"]["file"] == artifacts / "scripts" / manager / "salt-proxy"
    )
