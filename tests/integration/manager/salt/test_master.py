import tempfile

import yaml

from tests.integration.manager.salt.conftest import wait_for_log_message


def test_default_config(cli_raw_runpy):
    ret = cli_raw_runpy("salt.master", "--config-template")

    assert ret.json
    assert ret.json.heist.artifact_version == ""
    assert ret.json.heist.artifacts_dir.endswith("artifacts")
    assert ret.json.heist.auto_service is False
    assert ret.json.heist.checkin_time == 60
    assert ret.json.heist.clean is False
    assert ret.json.heist.dynamic_upgrade is False
    assert ret.json.heist.generate_keys is True
    assert ret.json.heist.key_plugin == "local_master"
    assert ret.json.heist.manage_service is None
    assert ret.json.heist.offline_mode is False
    assert ret.json.heist.onedir is False
    assert ret.json.heist.renderer == "yaml"
    assert ret.json.heist.retry_key_count == 5
    assert ret.json.heist.retry_key_count == 5
    assert ret.json.heist.roster is None
    assert ret.json.heist.roster_data is None
    assert ret.json.heist.roster_defaults == {}
    assert ret.json.heist.roster_dir.endswith("rosters")
    assert ret.json.heist.roster_file
    assert ret.json.heist.run_dir_root is False
    assert ret.json.heist.salt_repo_url == "https://repo.saltproject.io/salt/py3/"
    assert ret.json.heist.service_plugin == "raw"
    assert ret.json.heist.target == ""
    # assert ret.json.heist.tunnel_plugin == "asyncssh"


def test_help(cli_raw_runpy):
    """
    Verify that the salt minion heist manager is callable from the cli
    """
    ret = cli_raw_runpy("salt.master", "--help", parse_output=False)
    print(ret.stdout)


async def test_basic(
    cli_spawn,
    master_container,
):
    # Create a roster just for the salt master
    with tempfile.NamedTemporaryFile(suffix=".yml") as fh:
        fh.write(yaml.dump(dict(master=dict(master_container))).encode())
        fh.flush()
        proc_master = await cli_spawn("salt.master", "-R", fh.name, "--log-level=info")

        # Check the logs to verify that heist ran successfully for the master
        await wait_for_log_message(proc_master, "Opening SSH connection")

        # Wait for the binary to download and deploy, then start salt master
        await wait_for_log_message(proc_master, "Starting the service salt-master")

        # At this point everything is up and running perfectly
        await wait_for_log_message(proc_master, "Received exit status 0")
        await wait_for_log_message(proc_master, "Starting infinite loop")
