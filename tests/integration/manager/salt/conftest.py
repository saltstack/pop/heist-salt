import asyncio


async def wait_for_log_message(proc, msg: str, timeout: int = 10):
    acc = []
    try:
        while True:
            # Use asyncio.wait_for to enforce the timeout
            line = await asyncio.wait_for(proc.stderr.readline(), timeout=timeout)
            stderr = line.decode()
            acc.append(stderr)
            if msg in stderr:
                break
    except asyncio.TimeoutError:
        trace = "".join(acc)
        raise ValueError(f"Timeout waiting for log message: '{msg}':\n{trace}")
