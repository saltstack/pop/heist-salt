import asyncio
import os
import pathlib
import sys
import tempfile

import docker
import pytest
from dict_tools.data import NamespaceDict
from pytest_pop.plugin import cli_runpy

DOCKER_NETWORK = "heist-test-network"

# https://hub.docker.com/r/linuxserver/openssh-server
DOCKERFILE = b"""
FROM linuxserver/openssh-server:amd64-latest

# Use sed to modify sshd_config for PermitRootLogin and AllowTcpForwarding
RUN sed -i 's/^#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i 's/^AllowTcpForwarding no/AllowTcpForwarding yes/' /etc/ssh/sshd_config

# Set the default shell to bash for ez os detection
RUN sed -i 's|/bin/ash$|/bin/bash|' /etc/passwd

SHELL ["/bin/bash", "-c"]
"""

IMAGE_TAG = "ssh-server"


@pytest.fixture(scope="session", name="ssh_server")
def custom_ssh_server():
    if os.environ.get("GITLAB_CI") == "true":
        raise pytest.skip("These tests don't currently run in the pipeline")
    client = docker.from_env()
    # Create a temporary directory as build context
    with tempfile.TemporaryDirectory() as tmpdir:
        dockerfile_path = os.path.join(tmpdir, "Dockerfile")
        # Write the Dockerfile content to the Dockerfile within this directory
        with open(dockerfile_path, "wb") as fh:
            fh.write(DOCKERFILE)
        # Build the image using the directory as context
        client.images.build(path=tmpdir, tag=IMAGE_TAG, rm=True)
        yield IMAGE_TAG

    # Cleanup code after yielding
    # Stop and remove containers using the custom image
    for container in client.containers.list(all=True):
        if container.image.tags[0] == IMAGE_TAG:
            container.stop()
            container.remove()

    # Optionally, remove the custom image as well
    client.images.remove(IMAGE_TAG, force=True)


@pytest.fixture(name="network_name")
def docker_network_name() -> str:
    if os.environ.get("GITLAB_CI") == "true":
        # For running in docker-in-docker on gitlab-ci
        return "docker"
    else:
        # For running the tests locally
        return "bridge"


@pytest.fixture(name="host")
def docker_host() -> str:
    if os.environ.get("GITLAB_CI") == "true":
        # For running in docker-in-docker on gitlab-ci
        return "docker"
    else:
        # For running the tests locally
        return "localhost"


@pytest.fixture(name="hub", scope="function")
def integration_hub(hub) -> dict:
    hub.pop.sub.add(dyne_name="output")
    yield hub


@pytest.fixture(name="roster", scope="function")
def get_roster() -> dict:
    """
    Return a temporary file that can be used as a roster
    """
    roster = NamespaceDict()
    yield roster


@pytest.fixture(scope="function")
def minion_container(roster, network_name, host, ssh_server):
    client = docker.from_env()
    container = client.containers.run(
        ssh_server,
        command=["/bin/sh", "-c", "while true; do sleep 1; done"],
        detach=True,
        network=network_name,
        ports={"2222/tcp": 2222},
        hostname="minion",
        environment={
            "PUID": "0",
            "PGID": "0",
            "TZ": "Etc/UTC",
            "PASSWORD_ACCESS": "true",
            "USER_NAME": "root",
            "USER_PASSWORD": "pass",
        },
    )
    try:
        container.reload()
        with tempfile.TemporaryDirectory(prefix="heist_", suffix="_tests") as td:
            roster["minion"] = {
                "id": host,
                "port": 2222,
                "username": "root",
                "password": "pass",
                "known_hosts": None,
                "artifacts_dir": td,
            }
            yield roster["minion"]
    except KeyboardInterrupt:
        ...
    finally:
        container.stop()
        container.remove()


@pytest.fixture(scope="function")
def master_container(roster, network_name, host, ssh_server):
    client = docker.from_env()
    username = "root"
    password = "pass"
    port = 2223

    container = client.containers.run(
        ssh_server,
        # command=["/bin/sh", "-c", "while true; do sleep 1; done"],
        detach=True,
        network=network_name,
        ports={f"2222/tcp": port, "4505/tcp": 4505, "4506/tcp": 4506},
        hostname=host,
        environment={
            "PUID": "0",
            "PGID": "0",
            "TZ": "Etc/UTC",
            # "SUDO_ACCESS": "true",
            "PASSWORD_ACCESS": "true",
            "USER_NAME": username,
            "USER_PASSWORD": password,
        },
    )

    try:
        container.reload()

        roster["master"] = {
            "host": host,
            "id": host,
            "port": port,
            "username": username,
            "password": password,
            "known_hosts": None,
        }
        yield roster["master"]
    except KeyboardInterrupt:
        ...
    finally:
        container.stop()
        container.remove()


@pytest.fixture(name="cli_runpy", scope="function")
def get_cli_runpy(hub, roster):
    """
    Return a function that shells out to heist's run.py
    """
    runpy_path = pathlib.Path(__file__).parent / "run.py"
    assert runpy_path.exists(), f"Could not find a run.py at {runpy_path}"

    def _cli_runpy_wrapper(*args, check: bool = True, **kwargs):

        with tempfile.NamedTemporaryFile(
            prefix="roster-", suffix=".yaml", delete=True, mode="w+"
        ) as fh:
            # Add the roster data to the file
            if roster and not any("-R" in a for a in args):
                r = hub.output.yaml.display(roster)
                fh.write(r)
                fh.flush()
                args = [*args, "-R", fh.name]

            ret = cli_runpy(*args, runpy=str(runpy_path), **kwargs)

            if check:
                assert ret.retcode == 0, ret.stderr or ret.stdout

            if ret.json:
                if isinstance(ret.json, dict):
                    ret.json = NamespaceDict(ret.json)
            return ret

    yield _cli_runpy_wrapper


@pytest.fixture(name="cli_raw_runpy", scope="function")
def get_cli_raw_runpy(roster):
    """
    Return a function that shells out to heist's run.py
    """
    runpy_path = pathlib.Path(__file__).parent / "run_raw.py"
    assert runpy_path.exists(), f"Could not find a run.py at {runpy_path}"

    def _cli_runpy_wrapper(*args, check: bool = True, **kwargs):
        ret = cli_runpy(*args, runpy=str(runpy_path), **kwargs)

        if check:
            assert ret.retcode == 0, ret.stderr or ret.stdout

        return ret

    yield _cli_runpy_wrapper


async def cli_spawn(
    *args,
    env: dict = None,
):
    args = list(args)
    if env is None:
        env = {}

    runpy_path = pathlib.Path(__file__).parent / "run_raw.py"

    command = [sys.executable, str(runpy_path), *args]

    proc = await asyncio.create_subprocess_exec(
        *command,
        env=env,
        stderr=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
    )
    return proc


@pytest.fixture(name="cli_spawn", scope="function")
def get_cli_spawn():
    return cli_spawn
