def valid_service_names(hub):
    return (
        "salt-minion",
        "minion",
        "salt",
        "salt-master",
        "master",
        "proxy",
        "salt-proxy",
    )
