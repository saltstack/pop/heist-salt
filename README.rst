==========
heist-salt
==========

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-heist-teal
   :alt: Made with heist, a POP plugin to create network tunnels for distributing and managing agents
   :target: https://heist.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

About
=====

The whole point of Heist is to make deployment and management
of Salt easy!

Before you start please be advised that a more detailed quickstart is
available in the docs for `heist-salt <https://heist-salt.readthedocs.io/en/latest/>`__.

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/saltstack/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/saltstack/pop/pop-create/>`__

What is Heist?
--------------

This project is built with `Heist <https://heist.readthedocs.io>`__, a POP
plugin that creates network tunnels for distributing and managing agents. While
it has been originally built to deploy and manage Salt (``heist-salt``),
it can be used to distribute and manage other agents or plugins if extended to
do so.

Please see the Heist-Salt `getting started <https://heist-salt.readthedocs.io/en/latest/topics/heist-salt/getting-started.html>`__
to get quickly started with Heist-Salt.
