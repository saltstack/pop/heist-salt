=============
Salt Artifact
=============

Heist-salt uses `Heist <https://gitlab.com/saltstack/pop/heist>`_ to deploy and
manage the Salt artifact. The Salt binary Heist-Salt deploys is built using `relenv`_.
Relenv creates a reproducable and re-locatable python builds.

Heist automatically downloads artifacts from `repo.saltproject.io`_ and uses them to deploy
agents. Heist will automatically download the latest artifact from the repo, unless
it already exists in the `artifacts_dir` or a specific Salt version is set
via the `artifact_version` option. Heist will automatically detect the target
OS and download the appropriate binary. If `artifact_version` is set, heist
will download the `Salt` binary version from the repo if it exists.

.. note::
  Starting in Heist-Salt version v6.0.0, Heist will only support managing and deploying the
  Salt artifact 3006.0 and above.

You can deploy a custom version of Salt onedir package that includes a different version of python,
or more dependency libraries. See the :ref:`custom_artifact` documentation for more information.

When the artifacts are downloaded from the remote repo they are placed in
your `artifacts_dir`. By default this location is ``/var/tmp/heist/artifacts``

The downloaded executables are in a tarball and are versioned with a version number following
the dash right after the name of the binary. It also includes the OS and architecture.
In the case of `salt` the file looks like this: `salt-3006.0-onedir-linux-aarch64.tar.xz`.


.. _`salt-pkg`: https://gitlab.com/saltstack/open/salt-pkg
.. _`repo.saltproject.io`: https://repo.saltproject.io/salt/onedir/
.. _relenv: https://github.com/saltstack/relative-environment-for-python
