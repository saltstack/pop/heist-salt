.. _salt-master:

Heist Master
============

You can use the ``salt.master`` Heist plugin to deploy and manage
a Salt master artifact.


.. code-block:: bash

    heist salt.master -R roster.cfg

This will automatically download and deploy a Salt master to the
defined targets. This Heist manager will also handle the Salt master upgrades,
and managing the service.


If you want to define Salt master config options to add to the master config file,
you would define them in your roster file like so:

.. code-block:: yaml

    system_name:
      host: 192.168.1.2
      username: root
      password: "rootpassword"
      master_opts:
        log_level_logfile: debug
